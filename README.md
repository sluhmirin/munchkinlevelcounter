# README #

Simple Munchkin(tm) level counter with dice rolling.

Version: 0.5

Copyright © 2013 Sergejs Luhmirins <sergejs.luhmirins@gmail.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.