package lv.LevelCounter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class DialogNameChange extends DialogFragment {

    public static final String PLAYERNAME = "playername";
    private EditText mNewNameEdit;
    private ImageButton ibClear;
    private static CounterFragment sParent;


    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static DialogNameChange newInstance(String name, CounterFragment parent) {
        DialogNameChange f = new DialogNameChange();

        sParent = parent;

        Bundle args = new Bundle();
        args.putString(PLAYERNAME,name);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String name = getArguments().getString(PLAYERNAME);
        return new AlertDialog.Builder(getActivity())
                                .setTitle(getResources().getString(R.string.dialog_name_title))
                                .setView(inflateView(name))
                                .setNegativeButton(R.string.dialog_name_cancel, mOnCancelClick)
                                .setPositiveButton(R.string.dialog_name_ok, mOnOkClick)
                                .create();
    }

    private View inflateView(String name) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_name, null);
        mNewNameEdit = (EditText) v.findViewById(R.id.etNewName);
        ibClear = (ImageButton) v.findViewById(R.id.ibClearField);

        ibClear.setOnClickListener(mOnClearButtonClick);

        mNewNameEdit.setText(name);
        return v;
    }

    private DialogInterface.OnClickListener mOnCancelClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dismiss();
        }
    };

    private DialogInterface.OnClickListener mOnOkClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (mNewNameEdit.getText().length() > 0) {
                sParent.updateNameView(mNewNameEdit.getText().toString());
            } else {
                Toast.makeText(getActivity().getBaseContext(),
                        getResources().getString(R.string.dialog_name_empty_warning),
                        Toast.LENGTH_SHORT).show();
            }
            dismiss();
        }
    };

    private View.OnClickListener mOnClearButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mNewNameEdit.setText("");
        }
    };

}
