package lv.LevelCounter;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;


public class DialogDice extends DialogFragment {

    ImageView mDiceImage;
    Random mRand = new Random();

    private Handler mRollHandler = new Handler();
    private int mRollCounter = -1;
    private int mOldValue = 1;

    static DialogDice newInstance(){
        return new DialogDice();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.dialog_dice_rolling))
                .setView(inflateView())
                .setPositiveButton(getResources().getString(R.string.dialog_dice_close), mOnDoneClicked)
                .create();
    }

    private View inflateView() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_dice, null);
        mDiceImage = (ImageView) v.findViewById(R.id.ivDice);
        rollMore();
        return v;
    }

    private DialogInterface.OnClickListener mOnDoneClicked = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dismiss();
        }
    };

    private Runnable mDiceRoll = new Runnable() {
        @Override
        public void run() {
            int nextRolledNum = 1;
            do {
                nextRolledNum = mRand.nextInt(6) + 1;
            } while (nextRolledNum == mOldValue);
            mDiceImage.setImageDrawable(getNumberDrawable(nextRolledNum));
            rollMore();
        }
    };

    private void rollMore() {
        mRollCounter++;
        if (mRollCounter < 15) {
            mRollHandler.postDelayed(mDiceRoll,200);
        } else {
            getDialog().setTitle(getResources().getString(R.string.dialog_dice_done));
        }
    }

    private Drawable getNumberDrawable(int number) {
        int res = R.drawable.dice1;
        switch (number) {
            case 1: /* nothing to do here*/break;
            case 2:
                res = R.drawable.dice2;
                break;
            case 3:
                res = R.drawable.dice3;
                break;
            case 4:
                res = R.drawable.dice4;
                break;
            case 5:
                res = R.drawable.dice5;
                break;
            default:
                res = R.drawable.dice6;
                break;
        }
        return getResources().getDrawable(res);
    }

}
