package lv.LevelCounter;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CounterFragment extends Fragment {

    public static final int MAX_LEVEL = 10;
    private int mBGColor = Color.GRAY;

    private String mName = "Player ";
    private int mLevel = 1;

    private TextView mLevelLabel;
    private TextView mNameLabel;

    ImageButton mPlusButton;
    ImageButton mMinusButton;
    ImageButton mEditButton;



    public CounterFragment(int color, int num) {
        mBGColor = color;
        mName = mName + String.valueOf(num);
        Log.d(ActivityLevel.LOG_TAG, " Fragment created! ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(ActivityLevel.LOG_TAG, " Fragment onCreateView called ");

        View inflatedLayout = inflater.inflate(R.layout.fragment_counter, container, false);

        RelativeLayout bg = (RelativeLayout) inflatedLayout.findViewById(R.id.rlCounterContainer);
        bg.setBackgroundColor(mBGColor);

        mLevelLabel = (TextView) inflatedLayout.findViewById(R.id.tvLevel);
        mNameLabel = (TextView) inflatedLayout.findViewById(R.id.tvName);
        mPlusButton = (ImageButton) inflatedLayout.findViewById(R.id.btnPlus);
        mMinusButton = (ImageButton) inflatedLayout.findViewById(R.id.btnMinus);
        mEditButton = (ImageButton) inflatedLayout.findViewById(R.id.ibEdit);

        mNameLabel.setText(mName);
        updateLevelView();

        mPlusButton.setOnClickListener(mOnPlusClickListener);
        mMinusButton.setOnClickListener(mOnMinusClickListener);
        mEditButton.setOnClickListener(mOnEditClickListener);

        return inflatedLayout;
    }

    private View.OnClickListener mOnPlusClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mLevel < MAX_LEVEL) mLevel++;
            updateLevelView();
        }
    };

    private View.OnClickListener mOnMinusClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mLevel > 1) mLevel--;
            updateLevelView();
        }
    };

    private View.OnClickListener mOnEditClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment newFragment = DialogNameChange.newInstance(mName, CounterFragment.this);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            newFragment.show(fm, "dialog");
        }
    };

    public void updateNameView(String name) {
        mName = name;
        mNameLabel.setText(mName);
    }

    private void updateLevelView() {
        mLevelLabel.setText(String.valueOf(mLevel));
        if (mLevel == MAX_LEVEL) {
            mLevelLabel.setTextColor(getResources().getColor(R.color.text_winner));
        } else {
            mLevelLabel.setTextColor(getResources().getColor(R.color.text_dark));
        }

        // TODO Here should check if someone have won and send a callback.
        // TODO make epic mode possible.
    }

}
