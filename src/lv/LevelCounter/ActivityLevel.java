package lv.LevelCounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Random;

public class ActivityLevel extends FragmentActivity {

    public static final String PLAYER_COUNT = "player_count_label";

    public static final String LOG_TAG = "MegaLog";

    private int mPlayerCount = 3;
    private CounterFragment[] mListOfFragments;

    private int[] mRandomColors;
    private int mCurrentColor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);
        prepareColors();

        Intent intent = getIntent();
        mPlayerCount = intent.getIntExtra(PLAYER_COUNT, mPlayerCount);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        mListOfFragments = new CounterFragment[mPlayerCount];

        for (int ii = 0; ii < mListOfFragments.length; ii++) {
            mListOfFragments[ii] = new CounterFragment(getNextColor(), ii);
            fragmentTransaction.add(R.id.llFragmentContainer, mListOfFragments[ii]);
        }
        fragmentTransaction.commit();
    }

    private int getNextColor() {
        mCurrentColor = (mCurrentColor + 1) % mListOfFragments.length;
        return getResources().getColor(mRandomColors[mCurrentColor]);
    }

    /**
     * Creating and randomizing list of colors
     * Each game players have random backgrounds each time.
     */
    private void prepareColors() {
        mRandomColors = new int[]{ R.color.red, R.color.blue, R.color.cyan,
                                    R.color.green, R.color.orange, R.color.grey,
                                    R.color.purple, R.color.yellow};
        Random rnd = new Random();
        for (int i = mRandomColors.length - 1; i >= 0; i--) {
            int randomIndex = rnd.nextInt(i + 1);
            int tmp = mRandomColors[randomIndex];
            mRandomColors[randomIndex] = mRandomColors[i];
            mRandomColors[i] = tmp;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.actionRollDice:
                DialogDice dialogDice = DialogDice.newInstance();
                dialogDice.show(getSupportFragmentManager(), "dice_dialog");
                //Toast.makeText(this, "dice rolled", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
