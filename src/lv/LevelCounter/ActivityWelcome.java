package lv.LevelCounter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class ActivityWelcome extends Activity {

    TextView mPlayerCountLabel;
    SeekBar mSeekBar;

    public final int DEFAULT_USER_COUNT = 3;
    private int mCurrentCount = DEFAULT_USER_COUNT;

    private SeekBar.OnSeekBarChangeListener mOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mCurrentCount = DEFAULT_USER_COUNT + progress / getResources().getInteger(R.integer.step_of_slidebar);
            mPlayerCountLabel.setText(String.valueOf(mCurrentCount));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // Nothing here.
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // Nothing here.
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mPlayerCountLabel = (TextView) findViewById(R.id.tvPlayerCount);
        mSeekBar = (SeekBar) findViewById(R.id.sbPlayerCount);
        mSeekBar.setOnSeekBarChangeListener(mOnSeekBarChangeListener);
    }

    public void onStartGameClick(View v) {
        Intent intent = new Intent(v.getContext(), ActivityLevel.class);
        intent.putExtra(ActivityLevel.PLAYER_COUNT, mCurrentCount);
        startActivity(intent);
    }

}
